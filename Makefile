OUTDIR := .public
OUTFILE := $(OUTDIR)/index.html
INDIR := accomplishments/

all: $(OUTFILE) $(OUTFILE).gz

$(OUTFILE): $(INDIR)/* build.sh | $(OUTDIR)
	./build.sh $(INDIR) > $@

$(OUTFILE).gz: $(OUTFILE)
	gzip -f -k $^

$(OUTDIR):
	mkdir $(OUTDIR)

clean:
	- rm -rf $(OUTDIR)

.PHONY: all clean
