#!/bin/bash

# Build a single page static HTML website from each file in the input directory
# and print it to stdout.

# Effectively, the steps are as follows:
# 1. Output the HTML header and beginning of the body boilerplate
# 2. Sort the files in the input directory by file name, descending.
# 3. For each input file, output the date from the file name and then each line
#    contained within the file should go into an unordered HTML list so that it
#    is bulleted.
# 4. Output the ending HTML boilerplate.

set -e
#set -x

echo_usage() {
	echo "Usage: ${0} <inputdir>"
	echo
	echo "Output an HTML file to stdout containing inputs from inputdir"
	exit
}

process_input() {
	# $1 should be the directory to find files in
	# $2 should be the ISO date as a file name
	_INFILE=${1}/${2}
	_DATE=${2}
	echo "<p><b>${_DATE}</b></p>"
	echo "<ul>"
	while read line; do
		echo -n "<li>"
		# Replace anything that looks like a hyperlink with an actual one.
		# A hyperlink starts with http:// or https://, has 0 or more
		# non-whitespace characters followed by a word boundary.
		echo -n "${line}" | sed -e "s|\(https*://\S*\)\b|<a href=\"\1\">\1</a>|g"
		echo "</li>"
	done < ${_INFILE}
	echo "</ul>"
}

#########################
# Execution starts HERE #
#########################

if [ ${#} -ne 1 ]; then
	echo_usage
fi

# Output the boilerplate HTML header and basic CSS
echo "<html>"
echo "<meta charset=\"UTF-8\">"
echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
echo "<head>"
echo "<title>Andrew Bradford's Accomplishments</title>"
echo "<style type=\"text/css\">"
echo "font-family: sans-serif;"
echo "h1, h2, h3 { line-height: 1.2; }"
echo "body { margin: 40px auto; max-width: 1000px; line-height: 1.4; font-size: 16px; background-color: #FAFAFA; color: #040404; padding: 0 10px; }"
echo "</style>"
echo "</head>"
echo "<body>"
echo "<p><h2>I'm Andrew Bradford. These are some things that I've accomplished:</h2></p>"

# Get list of all files in input directory and sort them descending, send each
# one to be processed.
INPUTS=`ls -1 ${1}/ | sort -r`
for input in ${INPUTS}; do
	process_input ${1} ${input}
done

# Output closing HTML footer
echo "<p>For more info on me, see <a href=\"http://www.bradfordembedded.com/\">my blog</a>.</p>"
echo "<p>Copyright (C) Andrew Bradford</p>"
echo "</body>"
echo "</html>"
